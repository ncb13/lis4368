# LIS4368

## Nikhil Bosshardt

### Assignment 5

- Connected to local mySQL database
- Connection Pool useage
- Server side validation

[Local Link](http://localhost:9999/lis4368/customerList "Local Link")

1. Form with entered data:

![1](ss1.PNG)

2. Form with good data after submitting:

![2](ss2.PNG)

3. Database result:

![3](ss3.PNG)







