# LIS4368

## Nikhil Bosshardt

### Assignment 1-5, P1, P2 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Install Git

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Configure servlet options
    - Create hello world servlet
    - Create mySQL query servlet

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Set up database for petstore
    - Run SQL statements.
    - Export SQL statements.

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Working local LIS4368 Site
	- Working client side validation
	- New home page!

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Working local LIS4368 Site
    - Working server side validation
    - REGEX in java

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Connected to local mySQL database
    - Connection Pool useage
    - Server side validation

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Added customer viewing page with sortable rows
    - Added delete function
    - Added update function

