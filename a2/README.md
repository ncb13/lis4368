# LIS4368

## Nikhil Bosshardt

### Assignment 2


1. Testing links:
	- [Hello Dir listings](http://localhost:9999/hello)
	- [Hello homepage](http://localhost:9999/hello/index.html)
	- [Say Hello Servlet](http://localhost:9999/hello/sayhello)
	- [Query Book Servlet](http://localhost:9999/hello/querybook.html)
	- [Say Hi servlet](http://localhost:9999/hello/sayhi)


2. Screenshot of tomcat Querybook servlet running:


![hwapp Screenshot](img/ss1.PNG)

