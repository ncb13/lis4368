# LIS4368

## Nikhil Bosshardt

### Assignment 4

- Working local LIS4368 Site
- Working server side validation
- REGEX in java

[Local Link](http://localhost:9999/lis4368/customerform.jsp?assign_num=a4 "Local Link")

1. Form with unentered data:

![1](ss1.PNG)

2. Form with good data after submitting:

![2](ss2.PNG)

3. Result of totally failed data:

![3](ss3.PNG)







