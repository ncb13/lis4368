# LIS4368

## Nikhil Bosshardt

### Assignment 1


*1. Screen Shot of hwapp application running:*

![hwapp Screenshot](img/ss1.PNG)

*2. Screenshot of tomcat index.jsp running:*

![localhost Screenshot](img/ss2.PNG)

*3. Git commands w/short descriptions:*

1. git init - Creates an empty git repo.
2. git status - Shows the current repo status, what you have and have not sent etc.
3. git add - Adds a file to the index to be pushed/commited.
4. git commit - Record changes to your repo.
5. git push - Updates your remote repo.
6. git pull - Updates your local repo.
7. git rm - removes files from your index.

*4. Bitbucket Repo Links:*

1. [Tomcat LOCAL ONLY](http://localhost:9999/"LOCAL TOMCAT")
2. Bitbucket Tutorial - Station Locations:
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ncb13/bitbucketstationlocations/ "Bitbucket Station Locations")
3. Tutorial: Request to update a teammate's repository:
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ncb13/myteamquotes/ "My Team Quotes Tutorial")