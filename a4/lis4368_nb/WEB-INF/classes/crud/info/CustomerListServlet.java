// save as "<TOMCAT_HOME>\webapps\lis4368\WEB-INF\classes\crud\info\CustomerListServlet.java"
/*
1. Compile:
Windows:
  cd to C:\tomcat\webapps\lis4368\WEB-INF\classes
  javac -cp .;c:\tomcat\lib\servlet-api.jar crud/info/CustomerListServlet.java

Mac: 	
  cd to /Applications/tomcat/webapps/lis4368/WEB-INF/classes
  javac -cp .:/Applications/tomcat/lib/servlet-api.jar crud/info/CustomerListServlet.java

2. Run: http://localhost:9999/lis4368/customerList
*/
package crud.info;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
 
import crud.business.Customer;

//servlet CustomerList is mapped to the URL pattern /customerList. When accessing this servlet, it will return a message.
@WebServlet("/customerList")
public class CustomerListServlet extends HttpServlet
{
	//perform different request data processing depending on transfer method (here, Post or Get)
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
	{
		String url = "/index.jsp"; //initialize url value (used for logic below)
        
		// get current action
		String action = request.getParameter("action");
		if (action == null)
			{
				action = "join";  // default action
			}

        // perform action and set URL to appropriate page
		if (action.equals("join"))
			{
				url = "/customerform.jsp";    // the "join" page
			} 
		else if (action.equals("add"))
			{
				String firstName = request.getParameter("fname");
				String lastName = request.getParameter("lname");
				String street = request.getParameter("street");
				String city = request.getParameter("city");
				String state = request.getParameter("state");
				String zip = request.getParameter("zip");
				String phone = request.getParameter("phone");
				String email = request.getParameter("email");
				String balance = request.getParameter("balance");
				String total_sales = request.getParameter("total_sales");
				String notes = request.getParameter("notes");

				Pattern pname = Pattern.compile("([A-Za-z\\-]+)");
				Matcher mfname = pname.matcher(firstName);
				Matcher mlname = pname.matcher(lastName);
				Pattern pemail = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,}\\b");
				Matcher memail = pemail.matcher(email);
				Pattern pstreet = Pattern.compile("([A-Za-z0-9.\\s,\\-]+)");
				Matcher mstreet = pstreet.matcher(street);
				Pattern pcity = Pattern.compile("([A-Za-z.\\s,\\-]+)");
				Matcher mcity = pcity.matcher(city);
				Pattern pstate = Pattern.compile("([A-Z]+)");
				Matcher mstate = pstate.matcher(state);
				Pattern pzipphone = Pattern.compile("([0-9]+)");
				Matcher mzip = pzipphone.matcher(zip);
				Matcher mphone = pzipphone.matcher(phone);
				Pattern pdecimal = Pattern.compile("([0-9.]+)");
				Matcher mbalance = pdecimal.matcher(balance);
				Matcher mtotal_sales = pdecimal.matcher(total_sales);

				String message = " ";

				Customer user = new Customer(firstName, lastName, email, street, city, state, zip, phone, balance, total_sales, notes);

				if (firstName == null || lastName == null || email == null ||
						firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() ||
				street == null || city == null || state == null ||
						street.isEmpty() || city.isEmpty() || state.isEmpty() ||
				phone == null || zip == null || balance == null ||
						phone.isEmpty() || zip.isEmpty() || balance.isEmpty() ||
				total_sales == null || total_sales.isEmpty())
				{
					message += "All text boxes required except Notes. ";
					url = "/customerform.jsp";
				}

				if(!mfname.matches() && !firstName.isEmpty())
				{
					message += "First name can only contain letters and hyphens. ";
					url = "/customerform.jsp";
				}

				if(!mlname.matches() && !lastName.isEmpty())
				{
					message += "Last name can only contain letters and hyphens. ";
					url = "/customerform.jsp";
				}
				//Q
				if(!(email.contains("@") && email.contains(".")))
				{
					message += "Must include a valid email. ";
					url = "/customerform.jsp";
				}

				if(!mstreet.matches() && !street.isEmpty())
				{
					message += "Street can only contain letters, numbers, commas, or periods. ";
					url = "/customerform.jsp";
				}

				if(!mcity.matches() && !city.isEmpty())
				{
					message += "City can only contain letters, commas, or periods. ";
					url = "/customerform.jsp";
				}

				if(!mzip.matches() && !zip.isEmpty())
				{
					message += "Zip can only contain numbers. ";
					url = "/customerform.jsp";
				}

				if(!(zip.length() == 5 || zip.length() == 9))
				{
					message += "Zip must be either 5 or 9 digits. ";
					url = "/customerform.jsp";
				}

				if(!mphone.matches() && !phone.isEmpty())
				{
					message += "Zip can only contain numbers. ";
					url = "/customerform.jsp";
				}

				if(phone.length() != 10)
				{
					message += "Phone must be 10 digits. ";
					url = "/customerform.jsp";
				}

				if(!mbalance.matches() && !balance.isEmpty())
				{
					message += "Balance can only contain numbers and decimals. ";
					url = "/customerform.jsp";
				}

				if(balance.length() > 6)
				{
					message += "Balance cannot contain more than 6 digits including decimals. ";
					url = "/customerform.jsp";
				}

				if(!mtotal_sales.matches() && !total_sales.isEmpty())
				{
					message += "Total Sales can only contain numbers and decimals. ";
					url = "/customerform.jsp";
				}

				if(total_sales.length() > 6)
				{
					message += "Total Sales cannot contain more than 6 digits including decimals. ";
					url = "/customerform.jsp";
				}

				if(!mstate.matches() && !state.isEmpty())
				{
					message += "State can only contain capital letters. ";
					url = "/customerform.jsp";
				}

				if(state.length() != 2)
				{
					message += "State must be 2 characters. ";
					url = "/customerform.jsp";
				}

				if (message.equals(" "))
				{
					message = "";
					url = "/thanks.jsp";
				}
				request.setAttribute("user", user);
				request.setAttribute("message", message);
			}
		getServletContext()
			.getRequestDispatcher(url)
			.forward(request, response);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		doPost(request, response);
	}    
}
