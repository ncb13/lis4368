// save as "<TOMCAT_HOME>\webapps\lis4368\WEB-INF\classes\crud\business\Customer.java"
/*
 *After* making necessary changes, compile:
Windows:
  cd to C:\tomcat\webapps\lis4368\WEB-INF\classes
  javac -cp . crud/business/Customer.java

Mac:
  cd to /Applications/tomcat/webapps/lis4368/WEB-INF/classes
  javac -cp . crud/business/Customer.java
*/

package crud.business;

import java.io.Serializable;

//Reality-check: zip should be int, phone long, balance and totalSales BigDecimal data types
public class Customer implements Serializable
{
	private String fname;
	private String lname;
	private String street;
	private String city;
	private String state;
	private String zip;
	private String phone;
	private String email;
	private String balance;
	private String total_sales;
	private String notes;

	//default constructor
	public Customer()
	{
		fname = "";
		lname = "";
		street = "";
		city = "";
		state = "";
		zip = "";
		phone = "";
		email = "";
		balance = "";
		total_sales = "";
		notes = "";
	}

	public Customer(String parFirstName, String parLastName, String parEmail,
					String parStreet, String parCity, String parState, String parZip,
					String parPhone, String parBalance, String parTotal_sales, String parNotes)
	{
		this.fname = parFirstName;
		this.lname = parLastName;
		this.street = parStreet;
		this.city = parCity;
		this.state = parState;
		this.zip = parZip;
		this.phone = parPhone;
		this.email = parEmail;
		this.balance = parBalance;
		this.total_sales = parTotal_sales;
		this.notes = parNotes;
	}

	public String getFname()
	{
		return fname;
	}

	public void setFname(String parFirstName)
	{
		this.fname = parFirstName;
	}

	public String getLname()
	{
		return lname;
	}

	public void setLname(String parLastName)
	{
		this.lname = parLastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String parEmail)
	{
		this.email = parEmail;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTotal_sales() {
		return total_sales;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPhone() {
		return phone;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNotes() {
		return notes;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getState() {
		return state;
	}

	public void setTotal_sales(String total_sales) {
		this.total_sales = total_sales;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getStreet() {
		return street;
	}
}