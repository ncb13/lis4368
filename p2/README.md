# LIS4368

## Nikhil Bosshardt

### Project 2

    - Added customer viewing page with sortable rows
    - Added delete function
    - Added update function

[Local Link](http://localhost:9999/lis4368/customerAdmin "Local Link")

Form with entered data:

![1](ss1.PNG)

Form with good data after submitting:

![2](ss2.PNG)

Customer table page

![3](ss3.PNG)

Update page with modified data

![4](ss4.PNG)

Customer table page with new data

![5](ss5.PNG)

Delete functionality

![6](ss6.PNG)

Database changes

![7](ss7.PNG)









